package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class WelcomActivity extends AppCompatActivity {
    TextView deskripsi;
    ImageView logo;
    Animation lefttorightin, lefttorightout, righttolefin, righttoleft;
    FloatingActionButton but, btnprev;
    int x = 0;
    String pref_key = "First_Instal";
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom);
        lefttorightin = AnimationUtils.loadAnimation(this, R.anim.lefttorightin);
        lefttorightout = AnimationUtils.loadAnimation(this, R.anim.lefttorightout);
        righttolefin = AnimationUtils.loadAnimation(this, R.anim.righttoleftin);
        righttoleft = AnimationUtils.loadAnimation(this, R.anim.righttoleft);
        deskripsi = findViewById(R.id.tv_desk);
        logo = findViewById(R.id.logo);
        logo.setVisibility(View.VISIBLE);
        btnprev = findViewById(R.id.btnprev);
        but = findViewById(R.id.buttonnext);
        sharedPreferences = getSharedPreferences(pref_key, MODE_PRIVATE);
        if (!sharedPreferences.getBoolean(pref_key, true)) {
            Intent in = new Intent(this, LoginActivity.class);
            startActivity(in);
            finish();
        }
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x == 0) {

                    logo.startAnimation(righttoleft);
                    deskripsi.startAnimation(righttolefin);
                    logo.setVisibility(View.INVISIBLE);
                    deskripsi.setVisibility(View.VISIBLE);
                    btnprev.setVisibility(View.VISIBLE);
                } else if (x == 1) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean(pref_key, false);
                    editor.apply();
                    Intent i = new Intent(WelcomActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                x++;
            }
        });
        btnprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deskripsi.startAnimation(lefttorightout);
                logo.startAnimation(lefttorightin);
                logo.setVisibility(View.VISIBLE);
                deskripsi.setVisibility(View.INVISIBLE);
                btnprev.setVisibility(View.INVISIBLE);
                x--;
            }
        });
    }
}
