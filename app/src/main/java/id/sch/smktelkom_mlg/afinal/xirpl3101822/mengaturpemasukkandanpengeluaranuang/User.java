package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

/**
 * Created by Dina on 20/04/2018.
 */

public class User {
    private String Nama;
    private String Email;
    private String Password;

    public User() {

    }

    public User(String nama, String email, String password) {
        Nama = nama;
        Email = email;
        Password = password;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String username) {
        Email = Email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
