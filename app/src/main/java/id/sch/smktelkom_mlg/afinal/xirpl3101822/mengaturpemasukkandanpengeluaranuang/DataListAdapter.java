package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by Dina on 26/04/2018.
 */

public class DataListAdapter extends ArrayAdapter<Data> {

    FirebaseAuth mAuth;
    private Activity context;
    private int resource;
    private List<Data> listBulan;
    private DatabaseReference mDatabase;

    public DataListAdapter(Activity context, int resource, List<Data> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        listBulan = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.bulan_item, parent, false);

        final TextView textFolder = v.findViewById(R.id.textFolder);
        final CardView cardView = v.findViewById(R.id.cardView);

        textFolder.setText(listBulan.get(position).getBulan());

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                alertDialog1.setTitle("Pilih Action");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
                final String[] conten = {"Buka", "Hapus"};
                alertDialog1.setItems(conten, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String choice = conten[which].toString();
                        if (choice == "Hapus") {
                            mAuth = FirebaseAuth.getInstance();
                            String FB_Database_Path = mAuth.getCurrentUser().getUid();
                            mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path).child(listBulan.get(position).getId());
                            mDatabase.removeValue();

                        } else {
                            mAuth = FirebaseAuth.getInstance();
                            String FB_Database_Path = mAuth.getCurrentUser().getUid();
                            mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path);

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(listBulan.get(position).getId())
                                    .build();
                            mAuth.getCurrentUser().updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("", "USer ID set");
                                            }
                                        }
                                    });
                            Intent intent = new Intent(context, ToolbarActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("ID", listBulan.get(position).getId());
                            context.startActivity(intent);
                        }


                    }
                });

                alertDialog1.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                alertDialog1.show();

            }
        });

        return v;

    }

}
