package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ASUS on 4/30/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    Context context;
    List<HistoryModel> historylist;

    public HistoryAdapter(Context context, List<HistoryModel> historylist) {
        this.context = context;

        this.historylist = historylist;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, int position) {
        HistoryModel historyModel = historylist.get(position);
        holder.tanggal.setText("Tanggal : " + historyModel.getTanggal());
        holder.nominal.setText("Pengeluaran : " + historyModel.getPengeluaran());
        holder.barang.setText("Barang : " + historyModel.getBarang());
    }

    @Override
    public int getItemCount() {
        return historylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tanggal, nominal, barang;

        public ViewHolder(View itemView) {
            super(itemView);
            tanggal = itemView.findViewById(R.id.tv1);
            nominal = itemView.findViewById(R.id.tv2);
            barang = itemView.findViewById(R.id.tv3);
        }
    }
}
