package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TabunganActivity extends AppCompatActivity {
    FirebaseDatabase mDatabase;
    FirebaseAuth mAuth;
    RecyclerView rvTabungan;
    TextView tvTotal;
    TabunganAdapter adapter;
    List<Data> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabungan);
        mAuth = FirebaseAuth.getInstance();
        rvTabungan = findViewById(R.id.rvtabungan);
        rvTabungan.setLayoutManager(new LinearLayoutManager(this));
        tvTotal = findViewById(R.id.tvtotal);
        mDatabase = FirebaseDatabase.getInstance();
        mDatabase.getReference(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                int x = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    list.add(ds.getValue(Data.class));
                    x += Integer.valueOf(ds.getValue(Data.class).getDitabung());
                }
                adapter = new TabunganAdapter(TabunganActivity.this, list);
                adapter.notifyDataSetChanged();
                rvTabungan.setAdapter(adapter);
                tvTotal.setText("Total : " + x);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
