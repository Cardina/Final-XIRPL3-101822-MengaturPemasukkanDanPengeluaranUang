package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TargetFragment extends Fragment {

    public String masuk;
    FirebaseAuth mAuth;
    List<Data> datalist;
    DatabaseReference mDatabase;
    ProgressDialog progressDialog;
    View view;
    RadioGroup targetGroup;
    RadioButton radioHari, radioMinggu, targetGroupNb;
    String wtarget;
    String tabung;
    String target;
    String bulan;

    public TargetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_target, container, false);

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setMessage("Silahkan Tunggu...");
        progressDialog.show();

        targetGroup = view.findViewById(R.id.targetGroup);
        final TextView tvTarget = view.findViewById(R.id.tvTarget);
        radioHari = view.findViewById(R.id.radioHari);
        radioMinggu = view.findViewById(R.id.radioMinggu);
        final TextView wtargettext = view.findViewById(R.id.wtargettext);

        mAuth = FirebaseAuth.getInstance();
        datalist = new ArrayList<>();
        final String FB_Database_Path = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path + "/" + getActivity().getIntent().getStringExtra("ID"));
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                //fetch data from firebase
                Data data = dataSnapshot.getValue(Data.class);
                if (data != null) {
                    tvTarget.setText("Rp. " + data.getTarget());
                bulan = data.getBulan();
                masuk = data.getPemasukan();
                tabung = data.getDitabung();
                target = data.getTarget();
                wtarget = data.getwTarget();

                if (wtarget.equals("Perhari")) {
                    radioHari.setChecked(true);
                    Integer total = Integer.parseInt(target) / 30;
                    wtargettext.setText("Rp. " + Integer.toString(total));

                }
                if (wtarget.equals("Perminggu")) {
                    radioMinggu.setChecked(true);
                    Integer total = Integer.parseInt(target) / 4;
                    wtargettext.setText("Rp. " + Integer.toString(total));
                }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

        view.findViewById(R.id.btnSub).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gantiTarget();
            }
        });

        return view;
    }


    private void gantiTarget() {
        mAuth = FirebaseAuth.getInstance();
        String FB_Database_Path = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path);

        int selectedId = targetGroup.getCheckedRadioButtonId();
        targetGroupNb = view.findViewById(selectedId);


        Data data = new Data(bulan, getActivity().getIntent().getStringExtra("ID"), masuk, tabung, target, targetGroupNb.getText().toString());
        mDatabase.child(getActivity().getIntent().getStringExtra("ID")).setValue(data);

    }

}
