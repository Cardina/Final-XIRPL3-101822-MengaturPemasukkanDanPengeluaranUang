package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {
    EditText Email, Password, Nama;
    Button Daftar;
    User user;
    FirebaseDatabase database;
    DatabaseReference reference;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);
        Nama = findViewById(R.id.nama);
        Daftar = findViewById(R.id.btndaftar);
        mAuth = FirebaseAuth.getInstance();


        user = new User();
        Daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUser();
                mAuth.createUserWithEmailAndPassword(Email.getText().toString(), Password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(Nama.getText().toString()).build();
                            mAuth.getCurrentUser().updateProfile(profileChangeRequest);
                            mAuth.getCurrentUser().sendEmailVerification();
                            Intent i = new Intent(RegisterActivity.this, VerifikasiActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Toast.makeText(RegisterActivity.this, "Mohon Menunggu", Toast.LENGTH_SHORT).show();
                            startActivity(i);
                            finish();
                        }
                    }
                });
            }
        });
    }

    public void getUser() {
        user.setNama(Nama.getText().toString());
        user.setEmail(Email.getText().toString());
        user.setPassword(Password.getText().toString());
    }

}
