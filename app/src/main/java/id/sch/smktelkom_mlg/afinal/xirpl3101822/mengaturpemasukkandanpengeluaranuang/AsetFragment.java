package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AsetFragment extends Fragment {

    View view;
    TextView tvTarget, info;
    EditText editMasuk, editTabung;
    Button btnSimpan;
    FirebaseAuth mAuth;
    List<Data> datalist;
    ProgressDialog progressDialog;
    private DatabaseReference mDatabase;

    public AsetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_aset, container, false);
        editMasuk = view.findViewById(R.id.editMasuk);
        editTabung = view.findViewById(R.id.editTabung);
        tvTarget = view.findViewById(R.id.tvTarget);
        info = view.findViewById(R.id.info);
        btnSimpan = view.findViewById(R.id.btnSimpan);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpan();
            }
        });

        datalist = new ArrayList<>();
        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setMessage("Silahkan Tunggu...");
        progressDialog.show();

        mAuth = FirebaseAuth.getInstance();
        datalist = new ArrayList<>();
        final String FB_Database_Path = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path + "/" + getActivity().getIntent().getStringExtra("ID"));
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                //fetch data from firebase
                Data data = dataSnapshot.getValue(Data.class);
                if (data != null) {
                    info.setText(data.getBulan());
                    if (!data.getPemasukan().equals("0")) {
                        editMasuk.setText(data.getPemasukan());
                        editTabung.setText(data.getDitabung());
                        tvTarget.setText(data.getTarget());
                        info.setText(data.getBulan());

                        btnSimpan.setVisibility(View.INVISIBLE);
                        progressDialog.dismiss();
                    } else {
                        editMasuk.setHint("Masukan Nominal");
                        editTabung.setHint("Masukan Nominal");
                        tvTarget.setText("");
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });


        return view;

    }

    private void simpan() {

        int target = Integer.parseInt(editMasuk.getText().toString()) - Integer.parseInt(editTabung.getText().toString());
        String targetstring = Integer.toString(target);

        mAuth = FirebaseAuth.getInstance();
        String FB_Database_Path = mAuth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(FB_Database_Path);

        Data data = new Data(info.getText().toString(), getActivity().getIntent().getStringExtra("ID"), editMasuk.getText().toString(), editTabung.getText().toString(), targetstring, "pilih");
        mDatabase.child(getActivity().getIntent().getStringExtra("ID")).setValue(data);

        tvTarget.setText(data.getTarget());


    }

    public interface OnDataPass {
        void onDataPass(String data);
    }

}
