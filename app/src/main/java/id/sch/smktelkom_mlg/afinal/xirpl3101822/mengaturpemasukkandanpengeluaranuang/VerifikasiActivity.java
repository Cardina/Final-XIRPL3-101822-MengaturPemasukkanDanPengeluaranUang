package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class VerifikasiActivity extends AppCompatActivity {
    Button lanj, ver;
    TextView desk;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifikasi);

        lanj = findViewById(R.id.lanjut);
        ver = findViewById(R.id.kirim);
        mAuth = FirebaseAuth.getInstance();
        lanj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.getCurrentUser().reload();
                if (mAuth.getCurrentUser().isEmailVerified()) {
                    Intent i = new Intent(VerifikasiActivity.this, NavigasiActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Toast.makeText(VerifikasiActivity.this, "silahkan tunggu", Toast.LENGTH_SHORT).show();
                    startActivity(i);
                    finish();
                }
            }
        });
        ver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.getCurrentUser().sendEmailVerification();
            }
        });

    }
}
