package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PengeluaranFragment extends Fragment {
    View view;
    Button tang, simpan, tampil;
    EditText nominal, barang;
    TextView awal, tanggal;
    FirebaseAuth mAuth;
    List<Data> datalist;
    ProgressDialog progressDialog;
    int target;
    String barangString = "";
    int pengeluaran = 0;
    private DatabaseReference mDatabase;

    public PengeluaranFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pengeluaran, container, false);
        tampil = view.findViewById(R.id.btnhis);
        tang = view.findViewById(R.id.tomtang);
        nominal = view.findViewById(R.id.et_nominal);
        barang = view.findViewById(R.id.et_barang);
        tanggal = view.findViewById(R.id.et_tanggal);
        awal = view.findViewById(R.id.tvAwal);
        tampil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent h = new Intent(getContext(), TampilHistoryActivity.class);
                h.putExtra("Tanggal", tanggal.getText().toString());
                startActivity(h);
            }
        });
        tang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int tahun = c.get(Calendar.YEAR);
                int bulan = c.get(Calendar.MONTH);
                int hari = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        tanggal.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                        getData();

                    }
                }, tahun, bulan, hari);
                datePickerDialog.show();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(mAuth.getCurrentUser().getUid() + "/" + getActivity().getIntent().getStringExtra("ID"));
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Data data = dataSnapshot.getValue(Data.class);
                target = Integer.valueOf(data.getTarget());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        simpan = view.findViewById(R.id.btnSImpan);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpan();
            }
        });
        return view;
    }

    void getData() {
        FirebaseDatabase.getInstance().getReference(mAuth.getCurrentUser().getUid()).child(getActivity().getIntent().getStringExtra("ID")).child("history").child(tanggal.getText().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HistoryModel model = dataSnapshot.getValue(HistoryModel.class);
                if (model != null) {
                    pengeluaran = Integer.valueOf(model.getPengeluaran());
                    barangString = model.getBarang();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void simpan() {
        target -= Integer.parseInt(nominal.getText().toString());
        Map<String, Object> map = new HashMap<>();
        map.put("target", String.valueOf(target));
        FirebaseDatabase.getInstance().getReference(mAuth.getCurrentUser().getUid())
                .child(getActivity().getIntent().getStringExtra("ID"))
                .updateChildren(map);
        History history = new History();
        history.setBarang(barangString.concat("," + barang.getText().toString()));
        int total = pengeluaran + Integer.valueOf(nominal.getText().toString());
        history.setPengeluaran(String.valueOf(total));
        FirebaseDatabase.getInstance().getReference(mAuth.getCurrentUser().getUid()).child(getActivity().getIntent().getStringExtra("ID")).child("history").child(tanggal.getText().toString()).setValue(history);
        Toast.makeText(getContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
    }
}
