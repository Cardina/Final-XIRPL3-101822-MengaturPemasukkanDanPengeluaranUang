package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

/**
 * Created by Dina on 20/04/2018.
 */

public class Target {
    private String id;
    private String bulan;
    private String pemasukan;
    private String ditabung;
    private String target;

    public Target() {

    }

    public Target(String bulan, String id, String pemasukan, String ditabung, String target) {
        this.bulan = bulan;
        this.id = id;
        this.pemasukan = pemasukan;
        this.ditabung = ditabung;
        this.target = target;
    }

    public String getBulan() {
        return bulan;
    }

    public String getId() {
        return id;
    }

    public String getPemasukan() {
        return pemasukan;
    }

    public String getDitabung() {
        return ditabung;
    }

    public String getTarget() {
        return target;
    }
}
