package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

/**
 * Created by ASUS on 4/30/2018.
 */

public class HistoryModel {
    public String barang;
    public String pengeluaran;
    public String tanggal;

    public HistoryModel() {

    }

    public HistoryModel(String barang, String pengeluaran) {
        this.pengeluaran = pengeluaran;
        this.barang = barang;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getBarang() {
        return barang;
    }

    public void setBarang(String barang) {
        this.barang = barang;
    }

    public String getPengeluaran() {
        return pengeluaran;
    }

    public void setPengeluaran(String pengeluaran) {
        this.pengeluaran = pengeluaran;
    }
//
//    public Map<String, Object> toMap() {
//        HashMap<String, Object> result = new HashMap<>();
//
//        result.put("pengeluaran", pengeluaran);
//        result.put("barang", barang);
//
//        return result;
//    }
}
