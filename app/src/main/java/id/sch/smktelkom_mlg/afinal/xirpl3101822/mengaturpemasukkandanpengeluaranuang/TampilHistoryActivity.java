package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TampilHistoryActivity extends AppCompatActivity {

    String tanggal;
    DatabaseReference databaseReference;
    List<HistoryModel> list = new ArrayList<>();
    FirebaseAuth mAuth;
    private RecyclerView recyclerView;
    private HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_history);
        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference(mAuth.getCurrentUser().getUid());
        recyclerView = findViewById(R.id.recyler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        tanggal = getIntent().getStringExtra("Tanggal");
        databaseReference.child(mAuth.getCurrentUser().getDisplayName()).child("history").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    HistoryModel historyModel = dataSnapshot.getValue(HistoryModel.class);
                    historyModel.setTanggal(dataSnapshot.getKey());
                    list.add(historyModel);
                }
                if (list.size() > 0) {
                    Toast.makeText(TampilHistoryActivity.this, "tes", Toast.LENGTH_SHORT).show();
                }
                historyAdapter = new HistoryAdapter(TampilHistoryActivity.this, list);
                historyAdapter.notifyDataSetChanged();
                recyclerView.setAdapter(historyAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
