package id.sch.smktelkom_mlg.afinal.xirpl3101822.mengaturpemasukkandanpengeluaranuang;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SMK TELKOM on 01/05/2018.
 */

public class TabunganAdapter extends RecyclerView.Adapter<TabunganAdapter.ViewHolder> {

    Context context;
    List<Data> tabungan;

    public TabunganAdapter(Context context, List<Data> tabungan) {
        this.context = context;
        this.tabungan = tabungan;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_tabungan, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNama.setText(tabungan.get(position).getBulan());
        holder.tvNominal.setText("Rp. " + tabungan.get(position).getDitabung());
    }

    @Override
    public int getItemCount() {
        return tabungan.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNama, tvNominal;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNamaBulan);
            tvNominal = itemView.findViewById(R.id.tvNominalBulan);
        }
    }
}
